module mod1
    implicit none
    contains

    subroutine jacob(A, n, U)
        real :: A(1:n, 1:n), U(1:n, 1:n)
        integer i, n, j, imax, jmax
        real max, phi
        

        imax = 0
        jmax = 0
        max = -100  
        do i = 1, n
            do j = 1, n
                If  ( A(i,j) > max .and. i /= j) then
                    max = A(i,j) 
                    imax = i
                    jmax = j 
                endif
            enddo
        enddo

        
        if (imax /= jmax ) then
            phi = 0.5 * atan(2*A(imax, jmax)/( A(imax, imax) - A(jmax, jmax)))
        elseif (imax == jmax ) then
            phi = atan(1.0)
        endif

        !write(2,*) max,"max"
        do i = 1, n
            do j = 1, n
                U(imax, jmax ) = -sin(phi); U(jmax , imax) = sin(phi); U(imax, imax) = cos(phi); U(jmax, jmax) = cos(phi)
                if (i /= imax .and. j /= jmax .and. i == j) then
                    U(i, j) = 1
                elseif (i /= j) then
                    U(i, j) = 0
                endif
            enddo
        enddo

        !write(2, *) "Матрица вращения U:"
        !do i = 1, n
        !    write(2, *) (U(i, j), j = 1, n)
        !end do

        A = matmul(matmul(transpose(U), A), U)        
        

    end subroutine jacob



end module mod1