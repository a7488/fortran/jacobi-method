program jacobi
    use mod1
    implicit none
    integer, parameter :: n = 3
    real :: A(1:n, 1:n), U(1:n, 1:n), Sobst_vectors(1:n, 1:n), E(1:n, 1:n)
    real eps
    integer i,  j, k

    open(1, file = "input") 
    open(2, file = "result") 

    read(1, *) eps

    do i = 1, n
        read(1, *) (A(i, j), j = 1, n)
    end do

    write(2, *) "Точность:", eps
    write(2, *) "Исходная матрица А:"
    
    do i = 1, n
        write(2, *) (A(i, j), j = 1, n)
    end do
    
    E = 1
    do i = 1, n
        do j = 1, n
            if ( E(i, j) == 1 .and. i /= j) then       !делаю единичной
                 E(i, j) = 0
            endif
        enddo
    enddo

    k = 0
    do while ( sqrt( A(1,2)**2 + A(1,3)**2 + A(2,1)**2 + A(2,3)**2 + A(3,1)**2 + A(3,2)**2 ) > eps ) 
    !do i = 0, 2
        call jacob(A, n, U)   
        !write(2, *) "А на", i, "ой итерцаии"
        !do k = 1, n                                     
        !    write(2, *) (A(k, j), j = 1, n)
        !end do     
        Sobst_vectors = matmul(U, E) 
        E = U
        k = k + 1                                                                            
    enddo                                                           
    
    write(2, *) "k:", k
    write(2, *) "А после разложений"
    do k = 1, n                                     
        write(2, *) (A(k, j), j = 1, n)
    end do
    

    write(2, *) "Собственные числа матрицы A:" 
    do k = 1, n                                     
        write(2, *)"lambda = ", A(k, k)
    end do
 
    
    write(2, *) "Собственные векторы матрицы A:" 
    do k = 1, n                                     
        write(2, *)"v = ", "(",(Sobst_vectors(i,k), i = 1, n),")"
    end do


    !write(2, *) Sobst_vectors(1,1) * Sobst_vectors(1,2) + Sobst_vectors(2,1) * Sobst_vectors(2,2) + Sobst_vectors(3,1) * &
    !Sobst_vectors(3,2)

    !write(2, *) Sobst_vectors(1,1) * Sobst_vectors(1,3) + Sobst_vectors(2,1) * Sobst_vectors(2,3) + Sobst_vectors(3,1) * &
    !Sobst_vectors(3,3)

    !write(2, *) Sobst_vectors(1,2) * Sobst_vectors(1,3) + Sobst_vectors(2,2) * Sobst_vectors(2,3) + Sobst_vectors(3,2) * &
    !Sobst_vectors(3,3)

    !!!проверял ортогональность!!

end program jacobi